{ config, pkgs,lib,hyprland, ... }:
{
  imports =
    [
      ./hardware-configuration.nix
      ../../modules/development.nix
      ./cachix.nix
      ../../modules/stumpwm.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "victus"; # Define your hostname.
  networking.networkmanager.enable = true;
  programs.nm-applet.enable = true;
  networking.nameservers = [ "1.1.1.1" ];
  networking.networkmanager.wifi.scanRandMacAddress = false;

  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

  # Configure network proxy if necessary
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    # font = "Lat2-Terminus32";
    font = "ter-u24n";
    packages = with pkgs; [ terminus_font ];
    useXkbConfig = true;
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;

  # Enable the Plasma 6 Desktop Environment.
  services.displayManager.sddm.enable = true;
  services.desktopManager.plasma6.enable = true;

  # BUG: Conflict between plasma6 and sway, so I force this value
  programs.gnupg.agent.pinentryPackage = lib.mkForce pkgs.pinentry-gnome3;

  # Configure keymap in X11
  services.xserver.xkb.layout = "us";
  services.xserver.xkbOptions = "ctrl:swapcaps";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Using pipewire
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;

    jack.enable = true;
  };

  # brightness
  programs.light.enable = true;

  # Screen Sharing
  xdg.portal.wlr.enable = true;
  xdg.portal.enable = true;

  # Steam
  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };



  # Enable touchpad support (enabled default in most desktopManager).
  services.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.binarydigitz01 = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "input" "audio" "adb"];
  };

  programs.kdeconnect.enable = true;

  nixpkgs.overlays = [
    (final: prev: {
      steam = prev.steam.override ({ extraLibraries ? pkgs': [], ... }: {
        extraLibraries = pkgs': (extraLibraries pkgs') ++ ( [
          pkgs'.gperftools
        ]);
      });
    })
  ];

  environment.sessionVariables = rec {
    EDITOR = "emacsclient -c";
    BROWSER = "librewolf";
    NIXOS_OZONE_WL = "1";
    ANDROID_HOME = "/home/binarydigitz01/Android/Sdk";
  };

  # Include ~/bin/ in $PATH.
  environment.homeBinInPath = true;

  environment.systemPackages = with pkgs; [
    kde-gtk-config
    maliit-keyboard

    cachix
    vim
    # Libreoffice
    libreoffice-qt
    hunspell
    hunspellDicts.uk_UA
    hunspellDicts.th_TH
    hunspellDicts.en-us

    networkmanagerapplet
    wget
    htop
    krita
    aseprite-unfree
    alacritty
    starship
    tor-browser-bundle-bin

    scanmem

    #games
    r2modman
    prismlauncher
    lutris
    wine
    wine64
    winetricks
    protonup
    stockfish
    scid-vs-pc
    endless-sky
    airshipper
    xonotic
    wesnoth
    shattered-pixel-dungeon
    # manaplus

    #Multimedia
    obs-studio
    vlc
    libsForQt5.kdenlive
    calibre
    koreader

    #pipewire
    jconvolver

    # Midi music file
    qsynth
  ];

  services.flatpak.enable = true;

  services.syncthing = {
    enable = true;
    dataDir = "/home/binarydigitz01/Documents/org/";
    openDefaultPorts = true;
    configDir = "/home/binarydigitz01/.config/syncthing";
    user = "binarydigitz01";
    group = "users";
    guiAddress = "0.0.0.0:8384";
  };

  fonts = {
    fontDir.enable = true;
    packages  = with pkgs; [
      nerd-fonts.fira-code
      corefonts
      vistafonts
    ];
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
  };

  programs.ssh.startAgent = true;

  # Android Studio
  programs.adb.enable = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 123 3000 8080 ];
  networking.firewall.allowedUDPPorts = [ 123 3000 8080 ];
  # Or disable the firewall altogether.
  networking.firewall.enable = true;

  # Enabling this for dictionary
  environment.wordlist.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

  nixpkgs.config.allowUnfree = true;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];

  nix.settings.trusted-public-keys = [
    "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
  ];
  nix.settings.substituters = [
    "https://cache.iog.io"
  ];

  # CTF
  services.ntp = {
    enable = true;
    servers = [ "20.244.40.210" ];
  };
}
