{ config, pkgs,hyprland, ... }:
{
  imports =
    [
      ./hardware-configuration.nix
      ../../modules/development.nix
      ./cachix.nix
      ../../modules/hyprland.nix
    ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.networkmanager.enable = true;
  networking.networkmanager.insertNameservers = ["1.1.1.1"];

  # Set your time zone.
  time.timeZone = "Asia/Kolkata";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.wlo1.useDHCP = true;

  # Configure network proxy if necessary
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Enable the X11 windowing system.
  services.xserver.enable = true;


  # Enable the Plasma 5 Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;

  # Configure keymap in X11
  services.xserver.layout = "us";
  services.xserver.xkbOptions = "eurosign:e";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # For Emacs
  services.gnome.glib-networking.enable=true;


  # Using pipewire
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;

    jack.enable = true;
  };

  # brightness
  programs.light.enable = true;

  # Screen Sharing
  xdg.portal.wlr.enable = true;
  xdg.portal.enable = true;

  # Steam
  programs.steam = {
    enable = false;
    remotePlay.openFirewall = true; # Open ports in the firewall for Steam Remote Play
    dedicatedServer.openFirewall = true; # Open ports in the firewall for Source Dedicated Server
  };



  # Enable touchpad support (enabled default in most desktopManager).
  services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.binarydigitz01 = {
    isNormalUser = true;
    extraGroups = [ "wheel" "networkmanager" "video" "input" "audio"];
  };

  programs.kdeconnect.enable = true;

  environment.systemPackages = with pkgs; [
    kde-gtk-config
    maliit-keyboard

    iio-sensor-proxy # For auto rotation
    cachix
    vim
    # Libreoffice
    libreoffice-qt
    hunspell
    hunspellDicts.uk_UA
    hunspellDicts.th_TH
    hunspellDicts.en-us

    wget
    htop
    krita
    aseprite-unfree
    alacritty
    starship
    (tor-browser-bundle-bin.override {
      useHardenedMalloc = false;
    })

    #games
    lutris
    wine
    wine64
    winetricks
    protonup
    stockfish
    scid-vs-pc
    endless-sky
    airshipper
    xonotic
    wesnoth
    shattered-pixel-dungeon
    manaplus

    #Multimedia
    obs-studio
    vlc
    libsForQt5.kdenlive
    calibre

    #pipewire
    jconvolver
  ];

  services.syncthing = {
    enable = true;
    dataDir = "/home/binarydigitz01/Documents/org/";
    openDefaultPorts = true;
    configDir = "/home/binarydigitz01/.config/syncthing";
    user = "binarydigitz01";
    group = "users";
    guiAddress = "0.0.0.0:8384";
  };

  fonts.packages  = with pkgs; [
    (nerdfonts.override {fonts = [ "FiraCode" ];})
    corefonts
    vistafonts
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  programs.mtr.enable = true;
  programs.gnupg.agent = {
    enable = true;
  };

  programs.ssh.startAgent = true;

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 3000 8080 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  networking.firewall.enable = false;

  # Enabling this for dictionary
  environment.wordlist.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.11"; # Did you read the comment?

  nixpkgs.config.allowUnfree = true;

  # HACK for some reason, eventhough i don't need python 2.7, it is
  # refusing to build my flake. So i need this
  nixpkgs.config.permittedInsecurePackages = [
    "python-2.7.18.6"
  ];

  nix = {
    package = pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
'';
  };

  nix.settings.trusted-public-keys = [
    "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
  ];
  nix.settings.substituters = [
    "https://cache.iog.io"
  ];
}
