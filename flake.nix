{
  description = "My configuration for nix";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
    emacs-lsp-booster.url = "github:slotThe/emacs-lsp-booster-flake";
  };

  outputs = { self, nixpkgs, home-manager, emacs-overlay, ... }@inputs :
    let
      system = "x86_64-linux";

      pkgs = import nixpkgs {
        inherit system;
        config = {
          allowUnfree = true;
        };
      };

      lib = nixpkgs.lib;
    in
      {
        nixosConfigurations = {
          nixos = lib.nixosSystem {
            inherit system;

            modules = [
              ./hosts/nixos/configuration.nix
              home-manager.nixosModules.home-manager
              {
                home-manager.useGlobalPkgs = true;
                home-manager.users.binarydigitz01 = import ./home.nix;
              }
              {
                nixpkgs.overlays = [ emacs-overlay ];
              }
            ];

            specialArgs = {
              inherit inputs;
            };

          };

          victus = lib.nixosSystem {
            inherit system;

            modules = [
              ./hosts/victus/configuration.nix
              home-manager.nixosModules.home-manager
              {
                home-manager.useGlobalPkgs = true;
                home-manager.users.binarydigitz01 = import ./home.nix;
              }
              {
                nixpkgs.overlays = [ emacs-overlay.overlay ];
              }

            ];

            specialArgs = {
              inherit inputs;
            };

          };
        };
      };
}
