{ config, pkgs, ... }:
let
  lib = pkgs.lib;
  elvish-bash-wrapper = (pkgs.callPackage ./pkgs/elvish-bash-wrapper {});
in
{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "binarydigitz01";
  home.homeDirectory = "/home/binarydigitz01";
  home.language.base = "en_US.UTF-8";
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";

  services.emacs = {
    enable = true;
    startWithUserSession = "graphical";
  };

  programs.emacs = {
    enable = true;
    # HACK See https://github.com/nix-community/emacs-overlay/issues/312
    package = pkgs.emacs-unstable.overrideAttrs (old: {        # ⇐ added
      passthru = old.passthru // {
        treeSitter = true;
      };
    });


    extraPackages = epkgs: [
      epkgs.vterm
      epkgs.treesit-grammars.with-all-grammars
    ];
  };

  programs.java = {
    enable = true;
    package = (pkgs.jdk17.override { enableJavaFX = true; });
  };

  programs.bash = {
    enable = true;
    enableCompletion = true;

    bashrcExtra =
      ''
      # eval "$(direnv hook bash)"
      # eval "$(starship init bash)"
      '';

    historyIgnore = ["ls" "clear" "exit"];
  };

  services.mpd = {
    enable = true;
    musicDirectory = "${config.home.homeDirectory}/Music";
    extraConfig = ''
      audio_output {
        type "pipewire"
        name "My Pipewire output"
      }
    '';
  };


  xdg = {
    enable = true;
    configFile = {
      "i3/config".source = ./config-files/i3/config;
      "alacritty/alacritty.toml".source = ./config-files/alacritty/alacritty.toml;
      "doom" = {
        source = ./config-files/doom;
        recursive = true;
      };
      "nvim" = {
        source = ./config-files/nvim;
        recursive = true;
      };
      "emacs" = {
        source = ./config-files/emacs-config;
        recursive = true;
      };
      "sway" = {
        source = ./config-files/sway;
        recursive = true;
      };
      "hypr" = {
        source = ./config-files/hypr;
        recursive = true;
      };
      "waybar" = {
        source = ./config-files/waybar;
      };
      "waybar-hyprland" = {
        source = ./config-files/waybar-hyprland;
        recursive = true;
      };
    };
  };

  home.packages = with pkgs; [
    # Terminal Packages
    fd
    starship

    # GUI Apps
    inkscape
    librewolf-wayland
    thunderbird
    element-desktop

    aspellDicts.en
    emacs-lsp-booster

    ani-cli
    youtube-music
    discord
    dia

    #shell
    elvish
    elvish-bash-wrapper

    # Emacs
    etBook # Font for emacs
    glib-networking # for xwidgets
  ];

  programs.neovim = {
    enable = true;
    plugins = [ pkgs.vimPlugins.nvim-treesitter.withAllGrammars ];
  };
}
