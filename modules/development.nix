{config,pkgs, ...}:
{
  environment.systemPackages = with pkgs; [
    git
    nodejs
    mysql-workbench
    gcc
    gdb
    gnumake
    cmake
    godot_4
    direnv
    gcc
    glib
    python3
    clang
    clang-tools
    nodePackages.typescript
    firefox-devedition-bin

    # Common Lisp
    # sbcl.withPackages (ps: [ps.quicklisp])
    sbcl_2_4_6
    asdf
    racket
    guile
    guile-hoot

    # Rust
    rustup

    # Java
    maven

    # C#
    dotnet-sdk

    # Android Studio
    android-studio

    #Language servers
    cmake-language-server
    jdt-language-server
    nodePackages.typescript-language-server
    rust-analyzer

    # Command Line tools
    ripgrep
    fd
    zip
    unzip
    nix-index
    (aspellWithDicts (d: [d.en]))
  ];
}
