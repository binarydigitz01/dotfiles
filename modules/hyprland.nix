{ config, pkgs, inputs, ... }:
{

  nix.settings = {
    substituters = ["https://hyprland.cachix.org"];
    trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
  };

  programs.hyprland = {
    enable = true;
    package = inputs.hyprland.packages.${pkgs.stdenv.hostPlatform.system}.hyprland;
    xwayland.enable = true;
  };
  security.pam.services.swaylock.text = ''
    # Account management.
    account required pam_unix.so

    # Authentication management.
    auth sufficient pam_unix.so   likeauth try_first_pass
    auth required pam_deny.so

    # Password management.
    password sufficient pam_unix.so nullok sha512

    # Session management.
    session required pam_env.so conffile=/etc/pam/environment readenv=0
    session required pam_unix.so
  '';
  services.dbus.enable = true;

  xdg.portal = {
    enable = true;
    wlr.enable = true;
    extraPortals = [
      pkgs.xdg-desktop-portal-gtk
    ];
  };
  environment.systemPackages = with pkgs; [
    waybar
    light wofi wofi-emoji
    dracula-theme # gtk theme
    pkgs.adwaita-icon-theme # default gnome cursors    glib # gsettings
    swaylock
    swayidle
    grim # screenshot functionality
    slurp # screenshot functionality
    wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
    wayland-protocols
    wayland-utils
    xdg-desktop-portal-hyprland
    mako # notification system developed by swaywm maintainer
    keepassxc
    pulseaudio
    blueman
    wpaperd # Wallpaper

    playerctl
    # Music
    mpd mpc-cli ncmpcpp mpdris2
  ];

  # Bluetooth gui
  services.blueman.enable = true;
}
