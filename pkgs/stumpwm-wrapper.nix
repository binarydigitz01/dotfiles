{stdenv,sbcl_2_4_6,writeShellApplication}:
let
  sbclWithStumpwm = sbcl_2_4_6.withPackages (ps :  [
    ps.stumpwm
    ps.clx-truetype
    ps.xembed
    ps.slynk
    ps.dbus
  ]);
in
writeShellApplication {
  name = "stumpwm";
  runtimeInputs = [sbclWithStumpwm];
  text = ''
    ${sbclWithStumpwm}/bin/sbcl \
      --non-interactive \
      --eval '(require :asdf)' \
      --eval '(asdf:load-system :stumpwm)' \
      --eval '(stumpwm:stumpwm)'
  '';
}
