# This package was added to so that I could start elvish with environment varibles that could be
# inherited from bash, due to environment.variables doing some profile magic that only works with
# bash, see /etc/set-environment
{stdenv, bashInteractive, elvish}:
stdenv.mkDerivation (finalAttrs: {
    name = "evlish-starter";
    src = ./.;
    buildInputs = [ bashInteractive elvish ];
    installPhase = ''
      mkdir -p $out/bin
      cp elvish-bash-wrapper $out/bin
'';
  })
