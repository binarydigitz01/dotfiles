;;; binary-ttrpg.el --- A ttrpg utility module       -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(package-install 'hydra)
(package-vc-install "https://gitlab.com/howardabrams/emacs-rpgdm.git")

(Provide 'binary-ttrpg)
;;; binary-ttrpg.el ends here
