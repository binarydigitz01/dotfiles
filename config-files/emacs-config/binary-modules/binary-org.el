;;; binary-org.el --- org configuration              -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(setq org-directory "~/Documents/org/")

(setq ispell-program-name "aspell")

(eval-after-load "org"
  '(progn
     (setq org-todo-keywords '((sequence "TODO" "DONE"))
           org-log-done 'time
           org-startup-indented t
           org-startup-folded 'overview
           org-pretty-entities t
           org-hide-emphasis-markers t
           org-startup-with-inline-images t
           org-display-remote-inline-images 'download
           org-image-actual-width '(300))
     (add-to-list 'org-modules 'org-habit)
     (add-hook 'org-mode-hook (lambda () (display-line-numbers-mode 0)
                                (add-to-list 'completion-at-point-functions 'cape-dabbrev+dict)))

     ;; Setup common lisp for babel
     (org-babel-do-load-languages
      'org-babel-load-languages
      '((lisp . t)
        (emacs-lisp . t)))
     (setq org-babel-lisp-eval-fn 'sly-eval)))

;; Copied from stackoverflow, this retains colors for org src blocks and tables, while making them monospaced
(defun my-adjoin-to-list-or-symbol (element list-or-symbol)
  (let ((list (if (not (listp list-or-symbol))
                  (list list-or-symbol)
                list-or-symbol)))
    (require 'cl-lib)
    (cl-adjoin element list)))

(use-package org-appear
  :hook (org-mode . org-appear-mode))

(use-package olivetti
  :hook (org-mode . olivetti-mode))

(use-package org-super-agenda
  :init
  (setq org-super-agenda-groups
        '((:name "Important"
                 :priority "A"
                 :order 0
                 :tag "IMP")
          (:name "READ"
                 :tag "READ"
                 :priority "C"
                 :order 5)
          (:name "Nice To Do"
                 :tag "ntd"
                 :priority "D"
                 :order 6))))


(eval-after-load "org-agenda"
  '(progn (setq org-agenda-files '("~/Documents/org/"))
          (org-super-agenda-mode t)))

(use-package org-roam
  :init
  (setq org-roam-directory (concat org-directory "roam"))
  :config
  (org-roam-db-autosync-mode t))

(setq org-project-manager-default-project-library 'project)
(unless (package-installed-p 'org-project-manager)
  (package-vc-install "https://github.com/binarydigitz01/org-project-manager.git"))

;; capture setup
(setq org-capture-templates
      `(("t" "Todo" entry (file+headline ,(concat org-directory "todo.org") "Inbox")
	     "* TODO %?\n  %i\n  %a" :prepend t)
        ("b" "Blog Post" entry (file+headline
                                ,(concat org-directory "blog/content-org/all-posts.org") "Blog")
         "* TODO heading \n:PROPERTIES:\n:EXPORT_FILE_NAME: file-name\n:END:\n" :prepend t)))

(use-package ox-hugo
  :ensure t   ;Auto-install the package from Melpa
  :pin melpa  ;`package-archives' should already have ("melpa" . "https://melpa.org/packages/")
  :after ox)

(eval-after-load "org"
  `(when (display-graphic-p)
     (add-hook 'org-mode-hook 'variable-pitch-mode)

     ;; set fixed pitch to certain faces
     (mapc
      (lambda (face)
        (set-face-attribute
         face nil
         :inherit
         (my-adjoin-to-list-or-symbol
          'fixed-pitch
          (face-attribute face :inherit))))
      (list 'org-code 'org-block 'org-table))

     ;; I use org-alert to send notifications from my org agenda
     (use-package org-alert
       :custom
       ((alert-default-style 'notifications))
       :init
       (add-hook 'server-after-make-frame-hook
                 (lambda ()
                   (require 'org-alert)
                   (org-alert-enable)
                   (setq org-alert-notification-title "Org Agenda"))))
     ;; Faces setup

     ;; This part has been copied and modified from
     ;; https://zzamboni.org/post/beautifying-org-mode-in-emacs/
     (let* ((variable-tuple
             (cond ((x-list-fonts "ETBembo") '(:font "ETBembo"))))
            (headline           `(:weight bold)))

       (custom-theme-set-faces
        'user
        `(org-level-8 ((t (,@headline ,@variable-tuple))))
        `(org-level-7 ((t (,@headline ,@variable-tuple))))
        `(org-level-6 ((t (,@headline ,@variable-tuple))))
        `(org-level-5 ((t (,@headline ,@variable-tuple))))
        `(org-level-4 ((t (,@headline ,@variable-tuple :height 1.1))))
        `(org-level-3 ((t (,@headline ,@variable-tuple :height 1.25))))
        `(org-level-2 ((t (,@headline ,@variable-tuple :height 1.5))))
        `(org-level-1 ((t (,@headline ,@variable-tuple :height 1.75))))
        `(org-document-title ((t (,@headline ,@variable-tuple :height 2.0 :underline nil))))))

     (custom-theme-set-faces
      'user
      '(variable-pitch ((t (:family "ETBembo" :height 170 :weight thin))))
      '(fixed-pitch ((t ( :family "Fira Code Nerd Font" :height 130)))))))


(provide 'binary-org)
;;; binary-org.el ends here
