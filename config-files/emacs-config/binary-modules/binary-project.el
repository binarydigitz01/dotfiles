;;; binary-project.el --- Project configuration      -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defun bd/ignore-project-buffer (buffer)
  (not (buffer-file-name buffer)))

(setq project-ignore-buffer-conditions '(bd/ignore-project-buffer))

(defun bd/project-vterm ()
  "Start Vterm in the current project's root directory.
If a buffer already exists for running Vterm in the project's root,
switch to it.  Otherwise, create a new Vterm buffer.
With \\[universal-argument] prefix arg, create a new Vterm buffer even
if one already exists."
  (interactive)
  (defvar vterm-buffer-name)
  (let* ((default-directory (project-root (project-current t)))
         (vterm-buffer-name (project-prefixed-buffer-name "vterm"))
         (vterm-buffer (get-buffer vterm-buffer-name)))
    (if (and vterm-buffer (not current-prefix-arg))
        (pop-to-buffer vterm-buffer (bound-and-true-p display-comint-buffer-action))
      (vterm t))))

(defun binary-project/magit-status ()
  "Launch `magit-status' buffer in project-directory."
  (interactive)
  (let ((default-directory (project-root (project-current t))))
    (magit-status)))

(setq project-switch-commands
      '((?f "Find file" project-find-file)
	(?r "Find regexp" project-find-regexp)
	(?d "Find directory" project-find-dir)
	(?e "Eshell" project-eshell)
	(?v "Vterm" bd/project-vterm)
	(?m "magit-status" binary-project/magit-status)))

(setq project-vc-extra-root-markers (list ".dir-locals.el"))

;; xref

(setq xref-search-program #'ripgrep)

(provide 'binary-project)
;;; binary-project.el ends here
