;;; binary-tempel.el --- Tempel Configuration        -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file adds support for templates via tempel.el

;;; Code:

(use-package tempel
  :bind (("M-+" . tempel-complete) ;; Alternative tempel-expand
         ("M-*" . tempel-insert))
  :init
  ;; Setup completion at point
  (defun tempel-setup-capf ()
    (setq-local completion-at-point-functions
                (cons #'tempel-expand
                      completion-at-point-functions)))

  (add-hook 'conf-mode-hook 'tempel-setup-capf)
  (add-hook 'prog-mode-hook 'tempel-setup-capf)
  (add-hook 'text-mode-hook 'tempel-setup-capf)

  :config
  (define-key tempel-map (kbd "M-j") 'tempel-next)
  (define-key tempel-map [?\t] 'tempel-next)
  (define-key tempel-map (kbd "M-k") 'tempel-previous)
  (define-key tempel-map (kbd "C-M-j") 'tempel-end)
  (define-key tempel-map (kbd "C-M-k") 'tempel-beginning))

(defun binary/tempel-priority ()
  "Sets tempel at the beginning of `completion-at-point-functions'."
  (unless (not (member 'tempel-expand completion-at-point-functions))
    (setq-local completion-at-point-functions (remove 'tempel-expand completion-at-point-functions))
    (add-to-list 'completion-at-point-functions 'tempel-expand)))

(provide 'binary-tempel)
;;; binary-tempel.el ends here
