;;; binary-evil-keybindings.el --- All evil based Keybindings  -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

;; Magit Keybindings
(evil-global-set-key 'normal (kbd "<leader>gg") 'magit-status)
(evil-global-set-key 'normal (kbd "<leader>gl") 'magit-log)
(evil-global-set-key 'normal (kbd "<leader>gL") 'magit-log-buffer-file)
(evil-global-set-key 'normal (kbd "<leader>gb") 'magit-blame)

;; Toggle based keybindings
(evil-global-set-key 'normal (kbd "<leader>oa") 'org-agenda)
(evil-global-set-key 'normal (kbd "<leader>ot") 'bd/toggle-vterm)

;; Org Roam keybindings
(evil-global-set-key 'normal (kbd "<leader>nrf") 'org-roam-node-find)
(evil-global-set-key 'normal (kbd "<leader>nrc") 'org-roam-node-capture)

(evil-global-set-key 'normal (kbd "<leader>x") 'org-capture)

;; Code search based keybindings
(evil-global-set-key 'normal (kbd "<leader>si") 'consult-imenu)

;; File keybindings
(evil-global-set-key 'normal (kbd "<leader>ff") 'find-file)

;; Buffer keybindings
(evil-global-set-key 'normal (kbd "<leader>bd") 'kill-current-buffer)
(evil-global-set-key 'normal (kbd "<leader>bb") 'bd/switch-to-buffer)
(evil-global-set-key 'normal (kbd "<leader>bi") 'ibuffer)
(evil-global-set-key 'normal (kbd "<leader>bB") 'switch-to-buffer)
(evil-global-set-key 'normal (kbd "<leader>bj") 'bquick-add-buffer)
(evil-global-set-key 'normal (kbd "<leader>bk") 'bquick-switch-buffer)

(evil-global-set-key 'normal (kbd "<leader>p") project-prefix-map)
(evil-global-set-key 'normal (kbd "<leader>pnj") 'org-project-manager-open-node)
(evil-global-set-key 'normal (kbd "<leader>pnc") 'org-project-manager-capture)

(evil-define-key 'normal eglot-mode-map (kbd "<leader>cr") 'eglot-rename)
(evil-define-key 'normal eglot-mode-map (kbd "<leader>cf") 'eglot-format-buffer)

(evil-global-set-key 'motion (kbd "SPC") nil)
(evil-define-key 'normal dired-mode-map (kbd "SPC") nil)
(evil-define-key 'normal Info-mode-map (kbd "SPC") nil)

;; Emacs Lisp mode
(eval-after-load "binary-elisp"
  '(progn
     (evil-define-key 'normal 'emacs-lisp-mode-map (kbd "<localleader>eb") 'eval-buffer)
     (evil-define-key 'normal 'emacs-lisp-mode-map (kbd "<localleader>ed") 'eval-defun)
     (evil-define-key '(normal visual motion) 'emacs-lisp-mode-map (kbd "<localleader>er") 'eval-region)))

;; org mode
(eval-after-load "binary-org"
  '(progn
     (evil-define-key 'normal org-mode-map (kbd "<localleader>tt") 'org-todo)
     (evil-define-key 'normal org-mode-map (kbd "<localleader>tc") 'org-toggle-checkbox)

     (eval-after-load "org-super-agenda"
       '(progn
          (evil-add-hjkl-bindings org-super-agenda-header-map)
          (evil-add-hjkl-bindings org-agenda-keymap)))))

(provide 'binary-evil-keybindings)
;;; binary-evil-keybindings.el ends here
