;;; binary-corfu.el --- corfu module                 -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package corfu
  ;; Optional customizations
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  (corfu-preview-current nil)    ;; Disable current candidate preview
  (corfu-preselect 'first)      ;; Preselect the prompt
  (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  (corfu-scroll-margin 5)        ;; Use scroll margin
  (tab-always-indent 'complete)
  (corfu-separator ?\s)          ;; Orderless field separator
  :init
  (global-corfu-mode))


(use-package cape
  ;; Bind dedicated completion commands
  ;; Alternative prefix keys: C-c p, M-p, M-+, ...
  :bind (("C-c l p" . completion-at-point) ;; capf
         ("C-c l t" . complete-tag)        ;; etags
         ("C-c l d" . cape-dabbrev)        ;; or dabbrev-completion
         ("C-c l h" . cape-history)
         ("C-c l f" . cape-file)
         ("C-c l k" . cape-keyword)
         ("C-c l s" . cape-symbol)
         ("C-c l a" . cape-abbrev)
         ("C-c l l" . cape-line)
         ("C-c l w" . cape-dict)
         ("C-c l \\" . cape-tex)
         ("C-c l _" . cape-tex)
         ("C-c l ^" . cape-tex)
         ("C-c l &" . cape-sgml)
         ("C-c l r" . cape-rfc1345))
  :init
  (setq cape-dict-file (getenv "WORDLIST"))
  (defalias 'cape-dabbrev+dict
    (cape-capf-super #'cape-dabbrev #'cape-dict))

  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev+dict)
  :config
  (define-key corfu-map (kbd "C-j") #'corfu-next)
  (define-key corfu-map (kbd "C-k") #'corfu-previous))

(provide 'binary-corfu)
;;; binary-corfu.el ends here
