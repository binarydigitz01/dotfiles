;;; binary-misc.el --- Miscellaneous functions -*- dynamic-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file contains all custom created functions by me for convenience,
;; which are too short to exist in their own files.

;;; Code:

(defun binary-eshell/toggle-eshell ()
  (interactive)
  (let ((eshell-buffer-name (binary-eshell/eshell-buffer-name)))
    (if (binary-eshell/eshell-toggled-p)
        (delete-windows-on eshell-buffer-name)
      (progn
        (split-window-below)
        (other-window 1)
        (if (project-current)
            (let ((default-directory (project-root (project-current))))
              (eshell)))
        (eshell)))))

(defun binary-eshell/eshell-toggled-p ()
  "Checks if eshell is toggled."
  (let ((eshell-buffer-name (binary-eshell/eshell-buffer-name))
        (result))
    (dolist (element (window-list) result)
      (if (string= eshell-buffer-name (buffer-name (window-buffer element)))
          (setq result t)))
    result))

(defun binary-eshell/eshell-buffer-name ()
  "Returns the name of the eshell buffer. It works on the basis of the following rule:
If the current buffer is part of a project, then name it on the basis of the project,
else name it on the basis of default-directory."
  (let ((eshell-buffer-name))
    (if (project-current)
        (setq eshell-buffer-name
              (concat "*" (project-name (project-current)) "-eshell*"))
      (setq eshell-buffer-name (concat "*" default-directory "-eshell*")))))

(global-set-key (kbd"C-c o C-t") #'binary-eshell/toggle-eshell)

(defun view-text-file-as-info-manual ()
  "View ‘info’, ‘texi’, ‘org’, ‘md’ and 'NEWS' files as ‘Info’ manual."
  (interactive)
  (require 'rx)
  (require 'ox-texinfo)
  (when (buffer-file-name)
    (let* ((org-export-with-broken-links 'mark)
           (ext (file-name-extension (buffer-file-name))))
      (cond
       ;; A NEWS files
       ((string-match "NEWS" (file-name-nondirectory (buffer-file-name)))
        (with-current-buffer
            ;; NEWS files are likely to be in read-only directories.
            ;; So make a copy with an `.org' extension.  Most NEWS
            ;; file are `outline-mode' files with `org' like heading
            ;; structure.  Many of the recent files like ORG-NEWS are
            ;; proper `org' files.
            (find-file-noselect
             (make-temp-file
              (format "%s---" (file-name-nondirectory (buffer-file-name))) nil ".org"
              (buffer-substring-no-properties (point-min) (point-max))))
          (org-with-wide-buffer
           ;; `ox-texinfo' export fails if a headline ends with a
           ;; period (= ".").  So, strip those terminating periods.
           (goto-char (point-min))
           (while (re-search-forward (rx (and bol
                                              (one-or-more "*")
                                              " "
                                              (one-or-more any)
                                              (group ".")
                                              eol))
                                     (point-max) t)
             (replace-match "" t t nil 1))
           (goto-char (point-min))
           (while nil
             ;; TODO: If a NEWS file contains text which resemble a
             ;; LaTeX fragment, the `ox-texinfo' export wouldn't
             ;; succeed.  So, enclose the LaTeX fragment with Org's
             ;; verbatim `=' marker.
             )
           (save-buffer 0)
           (info (org-texinfo-export-to-info)))))
       ;; A `.info' file
       ((or (string= "info" ext))
        (info (buffer-file-name)))
       ;; A `.texi' file
       ((or (string= "texi" ext))
        (info (org-texinfo-compile (buffer-file-name))))
       ;; An `.org' file
       ((or (derived-mode-p 'org-mode)
            (string= "org" ext))
        (info (org-texinfo-export-to-info)))
       ;; A `.md' file
       ((or (derived-mode-p 'markdown-mode)
            (string= "md" ext))
        (let ((org-file-name (concat (file-name-sans-extension (buffer-file-name)) ".org")))
          (apply #'call-process "pandoc" nil standard-output nil
                 `("-f" "markdown"
                   "-t" "org"
                   "-o" ,org-file-name
                   ,(buffer-file-name)))
          (with-current-buffer (find-file-noselect org-file-name)
            (info (org-texinfo-export-to-info)))))
       (t (user-error "Don't know how to convert `%s' to an `info' file"
                      (buffer-file-name)))))))

(global-set-key (kbd "C-x x v") 'view-text-file-as-info-manual)

(defun binary/toggle-transparency ()
  (interactive)
  (let ((transparency (frame-parameter nil 'alpha-background)))
    (if (or (eq transparency 100) (eq transparency nil))
      (set-frame-parameter nil 'alpha-background 70)
    (set-frame-parameter nil 'alpha-background 100))))

(global-set-key (kbd "C-c o C-S-T") 'binary/toggle-transparency)

(provide 'binary-misc)
;;; binary-misc.el ends here
