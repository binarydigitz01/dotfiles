;;; binary-lsp.el --- Lsp Configuration              -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is my configuration for lsp.

;;; Code:

(defvar binary-lsp-package 'eglot)

(use-package lsp-mode)
(use-package eglot)

(defun binary-lsp-function ()
  (if (eq binary-lsp-package 'lsp)
      (lsp)
    (eglot-ensure)))

(defun binary/eglot-setup ()
  (when (eglot-managed-p)
      (binary/tempel-priority)))

(eval-after-load "eglot"
  (progn
    (add-hook 'eglot-managed-mode-hook 'binary/eglot-setup)
    (advice-add 'eglot-completion-at-point :around #'cape-wrap-buster)))

(unless (package-installed-p 'eglot-booster)
 (package-vc-install "https://github.com/jdtsmith/eglot-booster.git"))

(use-package eglot-booster
	:after eglot
	:config	(eglot-booster-mode))

(eval-after-load "lsp"
  (add-hook 'lsp-after-open-hook 'binary/tempel-priority))

(provide 'binary-lsp)
;;; binary-lsp.el ends here
