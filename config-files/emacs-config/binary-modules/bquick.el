;;; bquick.el --- Bquick extension                   -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: extensions, convenience, tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This file provides the bquick extension. Call `bquick-add-buffer'
;; to assign a buffer to a key. Call `bquick-switch-buffer' to switch
;; to a buffer that has been assigned.

;;; Code:

(defvar bquick-buffers-hash (make-hash-table :test 'equal :size 20)
  "Hashmap that contains all stored buffers.")

;;;autoload
(defun bquick-add-buffer ()
  "Add buffer to `bquick-buffers-hash'"
  (interactive)
  (let  ((key (read-key-sequence "Enter Key:")))
    (puthash key (current-buffer) bquick-buffers-hash)))

;;;autoload
(defun bquick-switch-buffer ()
  "Switch to a buffer that is in `bquick-buffers-hash'"
  (interactive)
  (let* ((key (read-key-sequence "Enter Key:"))
	 (buffer (gethash key bquick-buffers-hash)))
    (if buffer
	(switch-to-buffer buffer)
      (message "No buffer assigned to that key."))))

(provide 'bquick)
;;; bquick.el ends here
