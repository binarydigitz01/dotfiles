;;; binary-ui.el --- UI configuration                -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(use-package ef-themes
  :init
  (load-theme 'ef-elea-dark))

;; Font Setup
(defun binary/font-setup ()
  "Setup fonts."
  (set-face-attribute 'default nil :font "Fira Code Nerd Font" :height 140))

(defun binary/set-font-size (size)
  "Set the default font size to SIZE."
  (interactive "nEnter New Font Size: ")
  (set-face-attribute 'default nil :height size))

(if (daemonp)
    (add-hook 'server-after-make-frame-hook 'binary/font-setup)
  (add-hook 'window-setup-hook 'binary/font-setup))

(use-package ligature
  :init
  (add-hook 'prog-mode-hook 'ligature-mode)
  (add-hook 'text-mode-hook 'ligature-mode)
  :config
  ;; Enable the "www" ligature in every possible major mode
  (ligature-set-ligatures 't '("www"))
  ;; Enable traditional ligature support in eww-mode, if the
  ;; `variable-pitch' face supports it
  (ligature-set-ligatures 'eww-mode '("ff" "fi" "ffi"))
  ;; Enable all Cascadia Code ligatures in programming modes
  (ligature-set-ligatures 'prog-mode '("|||>" "<|||" "<==>" "<!--" "####" "~~>" "***" "||=" "||>"
                                       ":::" "::=" "=:=" "===" "==>" "=!=" "=>>" "=<<" "=/=" "!=="
                                       "!!." ">=>" ">>=" ">>>" ">>-" ">->" "->>" "-->" "---" "-<<"
                                       "<~~" "<~>" "<*>" "<||" "<|>" "<$>" "<==" "<=>" "<=<" "<->"
                                       "<--" "<-<" "<<=" "<<-" "<<<" "<+>" "</>" "###" "#_(" "..<"
                                       "..." "+++" "/==" "///" "_|_" "www" "&&" "^=" "~~" "~@" "~="
                                       "~>" "~-" "**" "*>" "*/" "||" "|}" "|]" "|=" "|>" "|-" "{|"
                                       "[|" "]#" ":=" ":>" ":<" "$>" "==" "=>" "!=" "!!" ">:"
                                       ">=" ">>" ">-" "-~" "-|" "->" "--" "-<" "<~" "<*" "<|" "<:"
                                       "<$" "<=" "<>" "<-" "<<" "<+" "</" "#{" "#[" "#:" "#=" "#!"
                                       "##" "#(" "#?" "#_" "%%" ".=" ".-" ".." ".?" "+>" "++" "?:"
                                       "?=" "?." "??" ";;" "/*" "/=" "/>" "__" "~~" "(*" "*)"
                                       "\\\\" "://")))


(setq display-line-numbers-type 'relative)
(add-hook 'text-mode-hook 'display-line-numbers-mode)
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(use-package helpful
  :init
  (global-set-key (kbd "C-h f") #'helpful-callable)
  (global-set-key (kbd "C-h v") #'helpful-variable)
  (global-set-key (kbd "C-h k") #'helpful-key)
  (global-set-key (kbd "C-h C") #'helpful-command))

(use-package hl-todo
  :init
  (global-hl-todo-mode))

(when (display-graphic-p)
  (setq jit-lock-defer-time 0.25 ;; For scrolling, emacs hangs if you hold C-v to scroll. This is a fix.
        next-screen-context-lines 10)

  (pixel-scroll-precision-mode 1)

  (setq pixel-scroll-precision-interpolate-page t
        pixel-scroll-precision-interpolation-total-time 0.75)

  (defun binary/pixel-scroll-interpolate-down ()
    "Interpolate a scroll downwards by one page."
    (interactive)
    (if pixel-scroll-precision-interpolate-page
        (pixel-scroll-precision-interpolate (- (window-text-height nil t))
                                            nil 0.75)
      (cua-scroll-up)))

  (defun binary/pixel-scroll-interpolate-up ()
    "Interpolate a scroll upwards by one page."
    (interactive)
    (if pixel-scroll-precision-interpolate-page
        (pixel-scroll-precision-interpolate (window-text-height nil t)
                                            nil 0.75)
      (cua-scroll-down)))

  (define-key pixel-scroll-precision-mode-map (kbd "C-v") #'binary/pixel-scroll-interpolate-down)
  (define-key pixel-scroll-precision-mode-map (kbd "M-v") #'binary/pixel-scroll-interpolate-up))

(unless (display-graphic-p)
  (defun binary/context-scroll-up ()
    "Scroll about 0.75 of current window in terminal."
    (interactive)
    (let ((lines (floor (* 0.75 (window-text-height)))))
      (scroll-up lines)))
  (defun binary/context-scroll-down ()
    "Scroll about 0.75 of current window in terminal."
    (interactive)
    (let ((lines (floor (* 0.75 (window-text-height)))))
      (scroll-down lines)))
  (define-key global-map (kbd "C-v") #'binary/context-scroll-up)
  (define-key global-map (kbd "M-v") #'binary/context-scroll-down)

  (setq next-screen-context-lines 5))


(provide 'binary-ui)
;;; binary-ui.el ends here
