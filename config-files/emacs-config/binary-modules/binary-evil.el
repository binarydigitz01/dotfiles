;;; binary-evil.el ---  -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Arnav Vijaywargiya
;;
;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Maintainer: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Created: February 18, 2023
;; Modified: February 18, 2023
;; Version: 0.0.1
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  This module contains configuration for evil mode.
;;
;;; Code:

(use-package evil
  :init
  (setq evil-want-keybinding nil
	evil-want-C-u-scroll t
	evil-want-keybinding nil
	evil-want-integration t
	evil-undo-system 'undo-redo
	evil-vsplit-window-right t
	evil-split-window-below t)
  (modify-syntax-entry ?_ "w")
  (evil-mode 1)
  :config
  (evil-set-leader 'normal (kbd "SPC"))
  (evil-set-leader 'normal (kbd "<leader>m") t))

(use-package evil-collection
  :init
  (evil-collection-init))

(use-package evil-commentary
  :init
  (evil-commentary-mode))

(use-package evil-escape
  :init
  (setq-default evil-escape-key-sequence "jk"
		evil-escape-delay 0.4
		evil-escape-excluded-states '(normal)
        evil-escape-excluded-major-modes '(org-agenda-mode))
  (evil-escape-mode))

(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode 1))

(eval-after-load "binary-org"
  '(progn
     (use-package evil-org
       :ensure t
       :after org
       :hook (org-mode . evil-org-mode)
       :config
       (require 'evil-org-agenda)
       (evil-org-agenda-set-keys)
       (evil-org-set-key-theme))))

(provide 'binary-evil)
