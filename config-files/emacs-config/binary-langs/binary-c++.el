;;; binary-c++.el --- C++ configuration              -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(add-to-list 'major-mode-remap-alist '(c-mode . c-ts-mode))
(add-to-list 'major-mode-remap-alist '(c++-mode . c++-ts-mode))
(add-to-list 'major-mode-remap-alist
	     '(c-or-c++-mode . c-or-c++-ts-mode))

(use-package quickrun)

(eval-after-load "c++-ts-mode"
  `(progn
     (define-key c++-ts-mode-map (kbd "C-x M-e") 'quickrun-shell)))

(provide 'binary-c++)
;;; binary-c++.el ends here
