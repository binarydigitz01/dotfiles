;;; binary-elisp.el --- Elisp configuration          -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: extensions

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(defun binary/elisp-setup ()
  ;; Adjusting imenu to include use-package
  (add-to-list 'imenu-generic-expression
               (list (purecopy "use-package")
	                 (purecopy (concat "^\\s-*("
			                   (regexp-opt
			                    '("use-package")
                                        t)
			                   "\\s-+'?\\(" (rx lisp-mode-symbol) "\\)"))
	                 2)
               ))
(eval-after-load "emacs-lisp-mode"
  `(add-hook 'emacs-lisp-mode-hook 'binary/elisp-setup))

(use-package rainbow-delimiters
  :hook (emacs-lisp-mode . rainbow-delimiters-mode))

(provide 'binary-elisp)
;;; binary-elisp.el ends here
