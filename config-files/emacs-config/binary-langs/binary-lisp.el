;;; binary-common-lisp.el --- Configuration for Common lisp  -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:
(defun binary/sly-mrepl-other-window ()
  "Open new or existing sly mrepl in other window."
  (interactive)
  (sly-mrepl 'switch-to-buffer-other-window))

(use-package sly
  :custom
  (inferior-lisp-program "sbcl")
  :config
  (setq-default sly-symbol-completion-mode nil)
  :bind (:map sly-mode-map
              ("C-c '" . binary/sly-mrepl-other-window)))

(use-package racket-mode)

(add-hook 'lisp-mode-hook #'rainbow-delimiters-mode)


(provide 'binary-lisp)
;;; binary-common-lisp.el ends here
