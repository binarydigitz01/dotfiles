;;; early-init.el --- early init -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2023 Arnav Vijaywargiya
;;
;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Maintainer: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Created: February 18, 2023
;; Modified: February 18, 2023
;; Version: 0.0.1
;; Keywords:
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  early init
;;
;;; Code:

;; Disable annoying gui features
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode -1)
(blink-cursor-mode -1)

(setq gc-cons-threshold 100000000) ;; 100mb
(setq read-process-output-max (* 1024 1024)) ;; 1mb

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(setq custom-file "~/emacs-custom.el")
(load-file custom-file)
