;;; init.el --- init file -*- dynamic-binding: t; -*-
;;
;; Copyright (C) 2023 Arnav Vijaywargiya
;;
;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Maintainer: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Created: February 18, 2023
;; Modified: February 18, 2023
;; Version: 0.0.1
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  This file contains general configuration for Emacs. For specific packages,
;;  look at binary-modules.
;;
;;; Code:init

(setq user-full-name "Arnav Vijaywargiya"
      user-mail-address "binarydigitz01@protonmail.com"

      frame-title-format '("%b")
      ring-bell-function 'ignore
      use-short-answers t

      ;; Turn off warnings as they're very annoying.
      native-comp-async-report-warnings-errors nil
      ;; When non-nil, some commands like 'describe-symbol' show more detailed
      ;; completions with more information in completion prefix and suffix.
      completions-detailed t
      ;; In addition to a fringe arrow, 'next-error' error may now optionally
      ;; highlight the current error message in the 'next-error' buffer.
      ;; This user option can be also customized to keep highlighting on all
      ;; visited errors, so you can have an overview what errors were already visited.
      next-error-message-highlight t
      uniquify-buffer-name-style 'forward

      backup-inhibited t

      ;; Setting vsplit the default of other-window.
      split-width-threshold 0
      split-height-threshold nil)

(setq-default fill-column 100)

;; Package setup
(require 'package)

(setq package-archives
      '(("elpa" . "https://elpa.gnu.org/packages/")
	    ("elpa-devel" . "https://elpa.gnu.org/devel/")
	    ("nongnu" . "https://elpa.nongnu.org/nongnu/")
	    ("melpa" . "https://melpa.org/packages/"))

      ;; Highest number gets priority (what is not mentioned has priority 0)
      package-archive-priorities
      '(("elpa" . 2)
	    ("nongnu" . 1)))

(package-initialize)

(require 'use-package)
(setq use-package-always-ensure t
      use-package-always-defer t)

;; A module system, inspired by protesilaos
(dolist (path '("binary-modules" "binary-langs"))
  (add-to-list 'load-path (locate-user-emacs-file path)))

(require 'binary-ui)
(require 'binary-dired)
(require 'binary-vertico)
(require 'binary-corfu)
(require 'binary-orderless)
(require 'binary-org)
(require 'binary-magit)
(require 'binary-project)
(require 'binary-lsp)
(require 'binary-tree-sitter)
(require 'binary-meow)
(require 'binary-erc)
(require 'binary-elfeed)
(require 'binary-prog)
(require 'bquick)
(require 'binary-misc)
(require 'binary-tempel)

(require 'binary-nix)
(require 'binary-elisp)
(require 'binary-lisp)
(require 'binary-rust)
(require 'binary-c++)
(require 'binary-gdscript)
(require 'binary-web)
(require 'binary-python)
(require 'binary-elvish)
(require 'binary-java)

(use-package envrc
  :init
  (envrc-global-mode 1))
(load custom-file)
;;; init.el ends here
