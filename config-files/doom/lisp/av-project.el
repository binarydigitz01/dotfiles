;;; ../../dotfiles/doom/.config/doom/lisp/av-project.el -*- lexical-binding: t; -*-

(map! :leader
      "p" '(:keymap project-prefix-map :which-key "Project"))

(setq project-switch-commands
      '((?f "Find file" project-find-file)
        (?r "Find regexp" project-find-regexp)
        (?d "Find directory" project-find-dir)
        (?e "Eshell" project-eshell)
        (?m "magit-status" magit-status)
        (?o "Open Org Node" org-project-manager-open-node)))

(provide 'av-project)
