;;; av-apps.el ---                                   -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords:

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(after! dired
  (setq dired-kill-when-opening-new-dired-buffer t))

(use-package! mastodon
  :config
  (map! :map mastodon-mode-map
        :n "j" #'mastodon-tl--goto-next-toot
        :n "k" #'mastodon-tl--goto-prev-toot
        :n "r" #'mastodon-tl--update
        :n "f" #'mastodon-tl--follow-user
        :n "F" #'mastodon-tl--unfollow-user
        :n "b" #'mastodon-toot--toggle-favourite
        :n "B" #'mastodon-toot--toggle-boost
        :n "t" #'mastodon-tl--thread)
  :custom
  ((mastodon-instance-url "https://emacs.ch")
   (mastodon-active-user "binarydigitz01")
   (mastodon-tl--show-avatars t)
   (mastodon-media--avatar-height 40)))


(use-package! elfeed
  :init
  (setq rmh-elfeed-org-files (list (concat doom-user-dir "elfeed.org"))))

;; Matrix setup with ement.el
(after! ement
  (map! :localleader :map ement-room-list-mode-map
        "j" #'ement-join-room))

(after! erc
  (add-to-list 'erc-modules 'notifications))

;; HACK raw.githubusercontent.com is banned in india, so make emojify not ask for
;; emojis
(setq emojify-download-emojis-p nil)

(provide 'av-apps)
;;; av-apps.el ends here
