;;; av-language-setup.el --- This file contains all my language setup and code editting tools  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: languages

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

;; elisp
(setq auto-insert t)


;; rust
(after! rustic
  ;; Set cargo watch command to clippy for better hints
  (setq lsp-rust-analyzer-cargo-watch-command "clippy"))


;;common lisp
(after! sly
  (setq sly-command-switch-to-existing-lisp 'always))

;; cc setup
(after! lsp-clangd
  (setq lsp-clients-clangd-args
        '("-j=3"
          "--background-index"
          "--clang-tidy"
          "--completion-style=detailed"
          "--header-insertion=never"
          "--header-insertion-decorators=0"))
  (set-lsp-priority! 'clangd 2))


(defun av/c-indent-complete()
  (interactive)
  (let (( p (point)))
    (c-indent-line-or-region)
    (when (= p (point))
      (call-interactively 'complete-symbol))))

(map! :map c-mode-base-map
      "<tab>" #'av/c-indent-complete)

;; Agressive indent
(global-aggressive-indent-mode t)
(add-to-list 'aggressive-indent-excluded-modes 'c++-mode)

(provide 'av-language-setup)
;;; av-language-setup.el ends here
