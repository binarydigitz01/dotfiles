;;; ../../dotfiles/doom/.config/doom/lisp/av-editing.el -*- lexical-binding: t; -*-

(map!
 "M-d" 'iedit-mode)

(add-hook! 'prog-mode-hook 'display-fill-column-indicator-mode 'column-number-mode)

(provide 'av-editing)
