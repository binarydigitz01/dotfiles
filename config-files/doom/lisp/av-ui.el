;;; ../../dotfiles/doom/.config/doom/lisp/av-ui.el -*- lexical-binding: t; -*-

(setq doom-font (font-spec :family "FiraCode Nerd Font Mono" :size 18 :weight 'semi-light))

(setq doom-theme 'modus-vivendi)

(setq display-line-numbers-type 'relative)

(blink-cursor-mode 0)

;; HACK Disable hl-line-mode
(add-hook 'hl-line-mode-hook (lambda () (global-hl-line-mode 0)))

(display-fill-column-indicator-mode 1)

(setq fancy-splash-image (concat doom-user-dir "banners/emacs23.svg"))

(setq highlight-indent-guides-responsive 'stack)

(provide 'av-ui)
