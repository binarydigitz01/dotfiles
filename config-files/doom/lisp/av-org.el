;;; av-org.el ---                                    -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Arnav Vijaywargiya

;; Author: Arnav Vijaywargiya <binarydigitz01@protonmail.com>
;; Keywords: lisp

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;;

;;; Code:

(setq org-directory "/home/binarydigitz01/Documents/org/")

(after! org
  (setq org-todo-keywords '((sequence "TODO" "DONE"))
        org-log-done 'time)
  (add-hook! 'org-mode-hook 'av/org-setup 'auto-fill-mode 'display-fill-column-indicator--turn-on))

(defun av/org-setup ()
  (setq-local fill-column 100))

(use-package! org-project-manager
  :init
  :custom
  ((org-project-manager-default-project-library 'project)))

(after! org-roam
  (setq org-roam-directory (concat org-directory "roam")))

(after! org-agenda
  (setq org-agenda-files '("~/Documents/org/"))
  (org-super-agenda-mode t))

(use-package! org-super-agenda
  :init
  (setq org-super-agenda-groups
        '((:name "Important"
           :priority "A"
           :order 0
           :tag "IMP")
          (:name "READ"
           :tag "READ"
           :priority "C"
           :order 5))))

(custom-set-faces!
  '(org-document-title :height 1.2))

(provide 'av-org)
;;; av-org.el ends here
