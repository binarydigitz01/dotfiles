;;; completion/icomplete/config.el -*- lexical-binding: t; -*-

(use-package! icomplete
  :hook ((doom-first-input . icomplete-vertical-mode)
         ;; HACK Marginalia has a wrapping problem with icomplete,
         ;; see https://github.com/minad/marginalia/issues/142
         (icomplete-minibuffer-setup . (lambda () (setq truncate-lines t))))
  :config
  (map! :map icomplete-minibuffer-map
        "C-j" #'icomplete-forward-completions
        "C-k" #'icomplete-backward-completions
        "<RET>" #'icomplete-force-complete-and-exit
        "M-<RET>" #'icomplete-ret))

(use-package! orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion)))))

(use-package! embark
  :defer t
  :init
  (setq which-key-use-C-h-commands nil)
  (map! [remap describe-bindings] #'embark-bindings
        "C-;"               #'embark-act  ; to be moved to :config default if accepted
        (:map minibuffer-local-map
         "C-;"               #'embark-act
         "C-c C-;"           #'embark-export
         "C-c C-l"           #'embark-collect
         :desc "Export to writable buffer" "C-c C-e" #'+vertico/embark-export-write)
        (:leader
         :desc "Actions" "a" #'embark-act))) ; to be moved to :config default if accepted

(use-package! marginalia
  :hook ((doom-first-input . marginalia-mode)
         (marginalia-mode . all-the-icons-completion-marginalia-setup))
  :config
  (pushnew! marginalia-command-categories
            '(+default/find-file-under-here . file)
            '(doom/find-file-in-emacsd . project-file)
            '(doom/find-file-in-other-project . project-file)
            '(doom/find-file-in-private-config . file)
            '(doom/describe-active-minor-mode . minor-mode)
            '(flycheck-error-list-set-filter . builtin)
            '(persp-switch-to-buffer . buffer)
            '(projectile-find-file . project-file)
            '(projectile-recentf . project-file)
            '(projectile-switch-to-buffer . buffer)
            '(projectile-switch-project . project-file)))
