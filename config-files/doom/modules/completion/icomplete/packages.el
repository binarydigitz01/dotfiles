;; -*- no-byte-compile: t; -*-
;;; completion/icomplete/packages.el

(package! orderless)

(package! consult)

(package! embark)
(package! embark-consult)

(package! marginalia)
(package! all-the-icons-completion)
