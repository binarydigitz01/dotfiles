;;; editor/tempel/config.el -*- lexical-binding: t; -*-

(use-package! tempel
  :init
  (defun tempel-setup-capf ()
    (setq-local completion-at-point-functions
                (cons #'tempel-complete
                      completion-at-point-functions)))

  (add-hook 'prog-mode-hook 'tempel-setup-capf)
  (add-hook 'text-mode-hook 'tempel-setup-capf)
  :custom
  (tempel-path (concat doom-user-dir "templates")))

(when (modulep! :completion corfu)
  (map! :map tempel-map
        "C-j" #'tempel-next
        "C-k" #'tempel-previous
        "RET" #'tempel-done)
  (map! :map corfu-map
        "C-t" #'tempel-complete))
