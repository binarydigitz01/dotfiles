(package! org-project-manager
  :recipe (:host github :repo "Ice-Cube69/org-project-manager"))

;; common lisp
(package! sly-quicklisp)
(package! sly-asdf)

;; better indent
(package! aggressive-indent)

;; apps
(package! mastodon)

;; Matrix
(package! ement)

(package! org-super-agenda)

(package! hl-line-mode :disable t)
