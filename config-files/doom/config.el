;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

(setq user-full-name "Arnav Vijaywargiya"
      user-mail-address "binarydigitz01@protonmail.com")



;; This is where all major configuration is.
(add-to-list 'load-path (concat doom-user-dir "lisp"))
(require 'av-ui)
(require 'av-language-setup)
(require 'av-org)
(require 'av-project)
(require 'av-apps)
(require 'av-editing)

(map!
 "C-s" #'consult-line)

(map! :leader
      (:prefix ("p n" . "org-project-manager")
               "j" #'org-project-manager-open-node
               "c" #'org-project-manager-capture-current))

(map! :leader
      "w" #'ace-window)

(after! corfu
  (setq corfu-auto nil)
  (map!
   :map corfu-map
   "C-j" #'corfu-next
   "C-k" #'corfu-previous))

(after! evil-escape
  (setq evil-escape-delay 0.3))
