(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("016f665c0dd5f76f8404124482a0b13a573d17e92ff4eb36a66b409f4d1da410" "02f57ef0a20b7f61adce51445b68b2a7e832648ce2e7efb19d217b6454c1b644" "944d52450c57b7cbba08f9b3d08095eb7a5541b0ecfb3a0a9ecd4a18f3c28948" default))
 '(elfeed-feeds '("https://planet.emacslife.com/atom.xml") t)
 '(emojify-emoji-set "twemoji-14.0.2")
 '(safe-local-variable-values
   '((eval setf
      (alist-get 'inextern-lang c-offsets-alist)
      0)
     (vc-prepare-patches-separately)
     (diff-add-log-use-relative-names . t)
     (vc-git-annotate-switches . "-w"))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ts-fold-replacement-face ((t (:foreground nil :box nil :inherit font-lock-comment-face :weight light)))))
(put 'projectile-ripgrep 'disabled nil)
(put 'projectile-grep 'disabled nil)
